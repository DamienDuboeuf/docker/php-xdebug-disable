ARG PHP_VERSION=7.0

FROM php:${PHP_VERSION}-alpine

MAINTAINER Damien DUBOEUF <duboeuf.damien@gmail.com>

run apk add git shadow

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions \
        xdebug

RUN rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

############
# Composer #
############

RUN curl -sS https://getcomposer.org/installer | php \
    && mv -v composer.phar /usr/local/bin/composer

########
# User #
########

ENV USER_ID=33
ENV USER_GID=33


COPY docker-user-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-user-entrypoint.sh"]
CMD ["php-fpm"]
