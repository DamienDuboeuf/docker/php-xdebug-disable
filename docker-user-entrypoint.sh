#!/bin/sh

if [ "$USER_ID" != "0" ]; then
	# Correct user-id and group-id
	usermod -o -u "$USER_ID" www-data
fi

if [ "$USER_GID" != "0" ]; then
	groupmod -o -g "$USER_GID" www-data
fi

if [ -f /usr/local/bin/docker-php-entrypoint ]; then
	docker-php-entrypoint "$@"
else
	exec "$@"
fi
