# Docker Image PHP XDebug

[![pipeline status](https://gitlab.com/DamienDuboeuf/docker/php-xdebug-disable/badges/master/pipeline.svg)](https://gitlab.com/DamienDuboeuf/docker/php-xdebug-disable/-/commits/master)

Its docker image php with XDebug for test your library

## Usages

```bash
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.0 sh # For PHP 7.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.1 sh # For PHP 7.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.2 sh # For PHP 7.2
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.3 sh # For PHP 7.3
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.4 sh # For PHP 7.4
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.0 sh # For PHP 8.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.1 sh # For PHP 8.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.2 sh # For PHP 8.2

docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:7.0-fpm sh # For PHP FPM 7.0
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:7.1-fpm sh # For PHP FPM 7.1
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:7.2-fpm sh # For PHP FPM 7.2
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:7.3-fpm sh # For PHP FPM 7.3
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:7.4-fpm sh # For PHP FPM 7.4
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:8.0-fpm sh # For PHP FPM 8.0
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:8.1-fpm sh # For PHP FPM 8.1
docker run -ti registry.gitlab.com/damienDuboeuf/docker/php-xdebug-disable:8.2-fpm sh # For PHP FPM 8.2
```

use specifique tagged version

```bash
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.0-v1.0.0 sh # For PHP 7.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.1-v1.0.0 sh # For PHP 7.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.2-v1.0.0 sh # For PHP 7.2
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.3-v1.0.0 sh # For PHP 7.3
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.4-v1.0.0 sh # For PHP 7.4
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.0-v1.0.0 sh # For PHP 8.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.1-v1.0.0 sh # For PHP 8.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.2-v1.0.0 sh # For PHP 8.2

docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.0-fpm-v1.0.0 sh # For PHP FPM 7.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.1-fpm-v1.0.0 sh # For PHP FPM 7.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.2-fpm-v1.0.0 sh # For PHP FPM 7.2
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.3-fpm-v1.0.0 sh # For PHP FPM 7.3
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:7.4-fpm-v1.0.0 sh # For PHP FPM 7.4
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.0-fpm-v1.0.0 sh # For PHP FPM 8.0
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.1-fpm-v1.0.0 sh # For PHP FPM 8.1
docker run -ti registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:8.2-fpm-v1.0.0 sh # For PHP FPM 8.2
```
